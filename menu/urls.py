from django.urls import path

from .views import IndexView, MenuView, HTMLMenuView

urlpatterns = [
    path(r"menu/", MenuView.as_view(), name="menu"),
    path(r"menu/html/", HTMLMenuView.as_view(), name="htmlmenu"),
    path(r"menu/html/<int:pk>/<backstack>/", HTMLMenuView.as_view(), name="htmlmenu"),
    path(r"menu/html/<int:pk>/", HTMLMenuView.as_view(), name="htmlmenu"),
    path(r"", IndexView.as_view(), name="index"),
]
