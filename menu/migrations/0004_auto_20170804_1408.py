# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-04 14:08
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("menu", "0003_auto_20170804_1407"),
    ]

    operations = [
        migrations.AlterField(
            model_name="image",
            name="kernel",
            field=models.URLField(),
        ),
    ]
