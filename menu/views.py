from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, DetailView
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.db.models import Q

from ipaddress import ip_address
from itertools import chain

from . import models


class IndexView(TemplateView):
    template_name = "menu/index.ipxe"


class MenuMixin(object):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        self.parameters = {"ip_address": ip_address(self.request.META["REMOTE_ADDR"])}
        return super().dispatch(*args, **kwargs)

    def get_submenus(self, menu):
        return []

    def get_object(self, queryset=None):
        if hasattr(self, "object"):
            return self.object

        if "pk" in self.kwargs:
            self.object = super().get_object(queryset=queryset)
            return self.object

        matching_menus = self.get_matching_menus()
        if len(matching_menus) == 1:
            self.object = matching_menus[0]
        elif len(matching_menus) > 1:
            self.object = models.Menu(
                pk=-1,
                title="Multiple menus available",
            )
            self.object.get_items = [
                models.Item(label=m.title, type="submenu", submenu=m)
                for m in matching_menus
            ]
        else:
            self.object = models.Menu(
                pk=-2,
                title="No menu available",
            )
            self.object.get_items = [
                models.Item(
                    label=self.object.title,
                    type="separator",
                ),
            ]
        return self.object

    def get_matching_menus(self):
        return [m.menu for m in models.Matcher.match(**self.parameters)]

    def get_context_data(self, *args, **kwargs):
        c = super().get_context_data(*args, **kwargs)

        c["submenus"] = self.get_submenus(self.get_object())
        c["view"] = self

        return c


class MenuView(MenuMixin, TemplateView):
    template_name = "menu/menu.ipxe"

    def post(self, *args, **kwargs):
        self.parameters.update(dict(self.request.POST.items()))
        return self.get(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        c = super().get_context_data(*args, **kwargs)

        c["menus"] = self.get_matching_menus()
        c["submenus"] = set(chain(*[self.get_submenus(m) for m in c["menus"]]))

        return c

    def get_submenus(self, menu):
        submenus = set()
        remaining = set()
        remaining.add(menu)
        while remaining:
            menu = remaining.pop()
            submenus.add(menu)
            item_list = menu.items.filter(Q(type="submenu") | Q(type="inline"))
            for item in item_list:
                if not item.submenu in submenus:
                    remaining.add(item.submenu)

        return submenus


class HTMLMenuView(MenuMixin, DetailView):
    model = models.Menu
    template_name = "menu/menu.html"
    context_object_name = "menu"

    def get_matching_menus(self):
        return models.Menu.objects.all()

    def get_back_url(self):
        if len(self.backstack) < 3:
            return reverse(
                "htmlmenu",
                kwargs={
                    "pk": self.backstack[-1].pk,
                },
            )
        return reverse(
            "htmlmenu",
            kwargs={
                "pk": self.backstack[-1].pk,
                "backstack": ",".join(
                    map(lambda m: str(m.id), self.backstack[1:-1]),
                ),
            },
        )

    def get_backstack(self):
        return ",".join(
            map(
                lambda m: str(m.id),
                self.backstack[1:] + [self.get_object()],
            )
        )

    def get(self, *args, backstack=None, **kwargs):
        self.backstack = [self.get_object()]
        if backstack is not None:
            for menu_id in backstack.split(","):
                try:
                    menu_id = int(menu_id)
                except ValueError:
                    break
                if menu_id < 0:
                    continue
                self.backstack.append(get_object_or_404(models.Menu, id=menu_id))
        return super().get(*args, **kwargs)
