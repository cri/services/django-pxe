"""django_pxe URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

# from social_django.urls import urlpatterns as social_django_urls
from django.contrib.auth import logout
from django.views.generic import RedirectView

urlpatterns = [
    # url(r'^admin/login', RedirectView.as_view(
    #    url='/login/epita/',
    #    permanent=True,
    #    query_string=True,
    # )),
    url(r"^admin/", admin.site.urls),
    # url(r'^', include('social_django.urls', namespace='social')),
    url(r"^logout/", logout, {"next_page": "/"}, name="logout"),
    url(r"^smmap/", include("smmap.urls")),
    url(r"^", include("menu.urls")),
]
