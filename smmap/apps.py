from django.apps import AppConfig


class SMMapConfig(AppConfig):
    labal = "smmap"
    name = "smmap"
    verbose_name = "SM Map"
