from django.views.generic import CreateView, TemplateView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse_lazy
from django.shortcuts import redirect

from .models import Machine


class Registered(TemplateView):
    template_name = "smmap/registered.ipxe"


class Register(CreateView):
    model = Machine
    fields = ("name", "domain", "uuid", "board_serial", "mac")
    template_name = "smmap/register.ipxe"
    success_url = reverse_lazy("smmap_registered")

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    # def form_valid(self, form, *args, **kwargs):
    #    from django.http import HttpResponse
    #    form.save()
    #    return HttpResponse('#!ipxe\necho -- saved\nread name\n')
