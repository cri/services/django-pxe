from django.db import models


class Machine(models.Model):
    name = models.CharField(
        max_length=256,
    )

    domain = models.CharField(max_length=256, blank=True)

    mac = models.CharField(
        max_length=17,
        unique=True,
    )

    uuid = models.UUIDField(
        blank=True,
        null=True,
    )

    board_serial = models.CharField(
        max_length=256,
        blank=True,
        null=True,
    )

    added_at = models.DateTimeField(
        auto_now_add=True,
    )

    class Meta:
        ordering = ("name",)
        unique_together = (("name", "domain"),)

    @classmethod
    def get_positions(cls, domain):
        machines = cls.objects.filter(domain=domain)
        groups = {}

        for m in machines:
            group = ""
            if "-" in m.name:
                group = m.name.split("-", 1)[1]
            groups.setdefault(group, []).append(m)

        positions = []
        rowoffset = 0
        maxp = {
            None: 0,
        }
        for group in sorted(groups.keys()):
            maxp[group] = None
            for (r, p), m in sorted([(m.get_position(), m) for m in groups[group]]):
                if r == 0:
                    r = 1
                if maxp[group] is None or p > maxp[group]:
                    maxp[group] = p
                positions.append((group, r, p, m))

        result = []
        offset = 1
        lastgroup = None
        lastrow = 0
        for (g, r, p, m) in sorted(positions):
            if g != lastgroup:
                offset += lastrow * maxp[lastgroup]
                lastgroup = g
                lastrow = 0
            if r > lastrow:
                lastrow = r
            result.append((g, r, p, m, offset + (r - 1) * maxp[g] + p))
        return result

    def get_position(self):
        name = self.name.split("-")[0]

        try:
            if len(name) == 6 and name[0:4:3] == "rp":
                return (int(name[1:3]), int(name[4:6]))

            if name[0] == "p":
                return (0, int(name[1:3]))
        except ValueError:
            pass

        raise ValueError("invalid machine name: '{}'".format(self.name))

    def __str__(self):
        return self.name
