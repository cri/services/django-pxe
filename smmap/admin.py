from django.contrib import admin
from django.http import HttpResponse

from .models import Machine

# TODO: add Network model
# from menu.models import Network

from pyaml import dump as yaml_dump


@admin.register(Machine)
class MachineAdmin(admin.ModelAdmin):
    list_display = ("name", "domain", "mac", "uuid", "board_serial")
    list_display_links = list_display
    list_filter = ("domain",)
    search_fields = list_display
    date_hierarchy = "added_at"
    actions = ("export_ldif", "wol")

    def export_ldif(self, request, queryset):
        domains = set()
        ldif = []
        counters = {}
        machines = set(queryset)
        for m in machines:
            domains.add(m.domain)
        for d in domains:
            lastid = None
            positions = Machine.get_positions(d)
            for g, r, p, m, i in sorted(positions):
                sm = m.domain.split(".")[0]
                network = Network.objects.get(name=sm).address
                ldif.extend(
                    [
                        "dn: cn={}.{},ou={},ou=hosts,dc=epita,dc=net".format(
                            m.name,
                            m.domain,
                            sm,
                        ),
                        "objectclass: device",
                        "objectclass: ieee802Device",
                        "objectclass: ipHost",
                        "cn: {}.{}".format(m.name, m.domain),
                        "ipHostNumber: {}.{}".format(
                            ".".join(network.split(".")[:-1]),
                            i,
                        ),
                        "macAddress: {}".format(m.mac),
                        "",
                    ]
                )

        response = HttpResponse("\n".join(ldif), content_type="text/ldif")
        response["Content-Disposition"] = 'attachment; filename="machines.ldif"'
        return response

    export_ldif.short_description = "Export selected as LDIF"

    def wol(self, request, queryset):
        domains = set()
        output = ""
        machines = set(queryset)
        for m in machines:
            domains.add(m.domain)
        for domain in domains:
            lastid = None
            output += "#{}\n".format(domain)
            for p, r, m in sorted(
                [(p, r, m) for g, r, p, m, i in Machine.get_positions(domain)]
            ):
                if lastid is None:
                    lastid = p
                if lastid != p:
                    lastid = p
                    output += "sleep 2\n\n"
                output += "wol {} net0 #{}\n".format(m.mac, m.name)

        output += "sleep 2\n"
        output += "goto _start"

        response = HttpResponse(output, content_type="text/plain")
        response["Content-Disposition"] = 'attachment; filename="wol.txt"'
        return response

    wol.short_description = "Export as an iPXE WoL script"
