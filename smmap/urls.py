from django.conf.urls import url, include

from .views import Register, Registered

urlpatterns = [
    url(r"^register/$", Register.as_view(), name="smmap_register"),
    url(r"^registered/$", Registered.as_view(), name="smmap_registered"),
]
